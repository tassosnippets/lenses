Notes

1. User can hover over any point in the live graph, if the number of messages at the time is less than 10 he can view each one in accordion style

2. Thought I could let user decide whether to view the graph at the heartbeat rate or the live feed one, didn't find the time so implemented only the live feed

3. Didn't use Bulma's tabs, why? Bulma? the documentation was a mess and even the examples of their tab component in their official site seemed broken  as I could not even toggle between tabs nevermind the non-existent community or any other available resources 

4. Could have payed more attention to the message keys for instance  use message timestamp (translated to a readable format) as valid keys but then again I noticed you didn't care that much, as is the case in your dashboard  in your offical lenses application

