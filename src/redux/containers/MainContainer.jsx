import React from 'react';
import { connect } from 'react-redux';
import { Actions as KafkaActions } from 'redux-lenses-streaming';

import Connect from '../components/Connect';
import Publish from '../components/Publish';
import Subscribe from '../components/Subscribe';
import MessageList from '../components/MessageList';
import {MessageGraph} from "../components/MessageGraph";
import { Tab } from 'semantic-ui-react'




class MainContainer extends React.Component {

  renderMesageList()
  {
    const { messages, commit } = this.props;
    return(
        <Tab.Pane  style={{ border: "none", boxShadow: "none" }}>
        <MessageList messages={messages} onCommitMessage={commit} />
          
        </Tab.Pane>
      )
  }

  render() {
    const { messages, commit } = this.props;
    let panes = [
      { menuItem: 'Message List', render:this.renderMesageList.bind(this)},
      { menuItem: 'Message Graph', render: () => <Tab.Pane> <MessageGraph /></Tab.Pane> } ,
    ]
    return (
      <div className="container app">
        <div className="columns">
          <div className="column">
            <Connect />
          </div>
          <div className="column">
            <Publish />
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <Subscribe />
              <Tab 
                renderActiveOnly={true}
                panes={panes}            
              />
          </div>
        </div>
      </div>
    );
  }
}

/**
 * Defaults and types
 */
MainContainer.defaultProps = {

};

MainContainer.propTypes = {
};

/**
 * Redux mappings
 */
const mapStateToProps = state => ({
  messages: state.session.messages,
});

const mapDispatchToProps = {
  ...KafkaActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);
