import React,{Component} from "react"
import { connect } from "react-redux"
import {MessageAltList} from "./AltListMessage"
import Moment from "moment"
import {Container,Accordion,Icon} from "semantic-ui-react"
import ReactHighChartManaged from "./ReactHighChartManaged"



class  AccordionMessages  extends Component
{ 

    state = { activeIndex: 0 }

    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index

        this.setState({ activeIndex: newIndex })
    }
    render()
    {
        const { activeIndex } = this.state
        const {messages} = this.props;
        return(
            <div>
            <Accordion>
              {
                messages&&Object.values(messages).length>10? null : messages&&Object.entries(messages).map(([key,message]) =>{

                const { activeIndex } = this.state
                return (
                        <div>
                            <Accordion.Title active={activeIndex === key} index={key} onClick={this.handleClick}>
                                 <Icon name='dropdown' />
                                { JSON.stringify(JSON.parse(message.key))}
                            </Accordion.Title>
                            <Accordion.Content active={activeIndex === key}>
                                 <MessageAltList heartBeatList={JSON.parse(message.value)} />
                             </Accordion.Content>
                       
                            </div>
                        )
                    }) 
                }
                </Accordion>
            </div>
            )
    }
}



class _MessageGraph extends Component{

	constructor()
	{
		super();
		this.heartBeat = 0;
		this.sdata =[];
		this.state = {messages : {},orderedMessages: {}, config: this.getInitialConfig()};
	}
 	getInitialConfig(title="live message graph",low=1,high=5){
  		var that = this;
        return {
          		// forceRefresh: true,
               chart: {
     			type: 'spline',
       			
      			 marginRight: 10,
       			 events: {
            	load: function () 
            	{
            	
            	var series = this.series[0];
                setInterval(function () {
                	
                	const orderedMessagesArray  = Object.entries(that.state.orderedMessages);
                
                	let leng = orderedMessagesArray.length;
                	if (leng == 0 )
                		leng=1;
                		// console.log("orderedMessagesArray",series.data[series.data.length-1],series.data[series.data.length-1].id ,orderedMessagesArray[leng-1][0])
                	if(series.data[series.data.length-1].id < orderedMessagesArray[leng-1][0])
                	{
                		
                		let y=Object.values( orderedMessagesArray[leng-1][1]).length;
                		let id=orderedMessagesArray[leng-1][0]
                			
                         var x = (new Date()).getTime(); // current time
                         series.addPoint({x:x,y:y,id:id}, true, true);
                	}
                	

                
                }, 1000);

		                 }
			        }
			    },
                title: {
                  
                    text: title,
                },
                   time: {
				        useUTC: false
				    },
                xAxis: {
                    type: "datetime",
                     tickPixelInterval: 150,
            
                },
                 responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 300
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                },
                yAxis: {
                    // min: low,
                    // max: high,
                    lineColor: '#FF0000',
                    title: {
                        text: `${title}`
                    },
                    plotLines: [{
			            value: 0,
			            width: 1,
			            color: '#808080'
			        }]

                },
                 legend: {
        				enabled: false
    			},
                tooltip: {
                    headerFormat:"{series.name}",
                    pointFormat: "{point.x:%e. %b}: {point.y:.2f}",
                },
                plotOptions: {
                    spline: {
                        marker: {
                            enabled: true,
                        },
                    },
                },
                  series: [
                    {
                        name: 'trololo',
                        data: (function () {
          
            var data = [],
                time = (new Date()).getTime(),
                i;

            for (i = -19; i <= 0; i += 1) {
                data.push({
                	id:0,
                    x: time + i ,
                    y: 0
                });
            }
            return data;
        }()),
                    
                 point: {
                            events: {
                           
                                mouseOver: (e) => {
                                    var  selfie =this;
                                    this.setState({pointX : e.target.plotX , pointY : e.target.plotY , pointId : e.target.id, display : "inline"})                                     
                                },
                 
                            },
                        },
                    },
                ],
        
    }
}
	componentWillReceiveProps(nextProps)
	{
		const now = Moment().format("hhmmss");

		this.setState( {orderedMessages: { ...this.state.orderedMessages, [`${now}`] : { ...nextProps.currentMessages,...this.state.orderedMessages[`${now}`] }   } } );
        if(Object.values(this.state.orderedMessages).length>3000)
            this.setState({orderedMessages : {} });
    
        let config = this.getInitialConfig('god',1,15);
	}


	render()
	{
		const messages  =  this.state.orderedMessages[this.state.pointId];
		return ( 
                <div className="hello" style={{width:"900px"}}>				
                    <Container style={{display:this.state.display,position:"absolute",left:this.state.pointX,top:this.state.pointY-200,zIndex:30000}}> <AccordionMessages messages={messages}/></Container>
    				<ReactHighChartManaged config = {this.state.config} />
		      	</div>
            )

	}
}

const mapStateToProps = ({session}) => {

	return {
		messages  : session.messages,
		currentMessages : session.currentMessages,
		heartbeat : session.heartbeatCount 
	}
}

export const  MessageGraph = connect(mapStateToProps)(_MessageGraph)

