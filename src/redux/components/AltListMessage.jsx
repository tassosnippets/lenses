import React,{Component} from "react"
import { List, Segment } from "semantic-ui-react"


export const MessageAltList = ({heartBeatList}) => {
	return (
		<Segment>
			<List > 
								
					{/*	Object.entries(JSON.parse(message.value)).map([val,key] => ( */}
						
							{Object.entries(heartBeatList).map( ([key,val])=>(
								<List.Item key={key}>
									<List.Content>
										<List.Header>{key}</List.Header>
										<List.Description>{val}</List.Description>
									</List.Content>
								</List.Item>	
								))}
																			
			</List>
		</Segment>
		)
}