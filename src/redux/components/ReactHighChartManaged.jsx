import React,{Component} from "react"
import { connect } from "react-redux"
import {MessageAltList} from "./AltListMessage"
import Moment from "moment"
import ReactHighCharts from "react-highcharts"

export default class ReactHighChartManaged extends Component{

	shouldComponentUpdate(nextProps,nextState)
	{
		return false;
	}

	render()
	{
		return (
			<ReactHighCharts config = {this.props.config} />)
	}

}